import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import PerfectScrollbar from 'vue2-perfect-scrollbar'
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css'
import '@babel/polyfill'
import VuetifyToast from './components/Global/Toast'
import VueApexCharts from 'vue-apexcharts'

var VueEventBus = require('vue-event-bus')
Vue.use(VueEventBus)

Vue.component('apexchart', VueApexCharts)

Vue.config.productionTip = false

Vue.use(PerfectScrollbar)
Vue.use(VuetifyToast, {
  y: 'top',
  showClose: true,
  queueable: true
})

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
