import Vue from 'vue'
import VueRouter from 'vue-router'
import Auth from "../views/Layouts/Layout_Auth";
import Login from "../components/Auth/Login";
import Home from "../views/Layouts/Layout_Main";
import Cabinero from "../views/Pages/Cabinero";
import Ajustador from "../views/Pages/Ajustador";
import Supervisor from "../views/Pages/Supervisor";
import Compras from "../views/Pages/Compras";
import Comandas from "../views/Pages/Ordenes";
import Dashboard from "../views/Pages/Dashboard";
import {goTo} from "vuetify";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/auth/login'
  },
  {
    path: '/auth',
    name: 'auth',
    component: Auth,
    children: [
        {
          path: 'login',
          name: 'login',
          component: Login
        }
    ]
  },
  {
    path: '/app',
    name: 'app',
    component: Home,
    children: [

        {
            path: 'Dashboard',
            name: 'Dashboard',
            component: Dashboard
        },


      {
        path: 'Cabinero',
        name: 'Cabinero',
        component: Cabinero
      },


      {
        path: 'Ajustador',
        name: 'Ajustador',
        component: Ajustador
      },

      {
        path: 'Supervisor',
        name: 'supervisor',
        component: Supervisor
      },


      {
        path: 'Ordenes de compra',
        name: 'Compras',
        component: Compras
      },
      {
        path: 'Comandas',
        name: 'Comandas',
        component: Comandas
      }
    ]
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior: (to, from, savedPosition) => {
    let scrollTo = 0

    if (to.hash) {
      scrollTo = to.hash
    } else if (savedPosition) {
      scrollTo = savedPosition.y
    }

    return goTo(scrollTo)
  },
  routes
})

export default router
