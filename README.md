<img src="src/assets/branding/poweredby.png" alt="PoweredBy" width="30%"/>

# Plantilla Prianti para maquetación
Plantilla creada para maquetación y desarrollo de aplicaciónes de Prianti, esta plantilla solo contiene la estructura basica
de un proyecto Front End, de no estar familiarizado con el desarrollo de apps con Vue es necesario leer la documentación 
de las dependenciás principales para entender como agregar vistas, componentes y demas.

### Dependecias principales ( Documentación )
* [Vue](https://vuejs.org/)
    * [Vuex](https://vuex.vuejs.org/)
    * [Vue-Router](https://router.vuejs.org/)
* [Vuetify](https://vuetifyjs.com/es-MX/)
* [Material Design Icons](https://materialdesignicons.com/)

### Estructura de la plantilla
- _dist_ - Esta carpeta es creada automaticamente al compilar la plantilla para producción
- public - Contiene los archivos estaticos de la plantilla en este caso solo el index.html y el favicon
- src - Codigo fuente de la plantilla
    - assets - Contiene imagenes que queremos que pasen por Webpack
    - components - Aqui van los componentes que conforman nuestra App
    - plugins - Plugins de Vue
        - vuetify.js - Archivo de configuración de vuetify
    - router - Aqui definimos las rutas de nuestra app
    - store - Aqui definimos como vamos a manejar el estado de nuestra app
    - utils - Utilidades, actualmente solo contiene la definición del menu
    - views - Vistas principales o Layouts
    - App.vue - Vista principal de Vue
    - main.js - Archivo de entrada de Vue este archivo en este archivo se inicializa vue y se configura.
    
### Antes de empezar
**Antes que nada es necesario que al clonar el repositorio quitemos el origen y agregemos un nuevo origen con la finalidad de no mandar cambios de branding u parecidos a esta plantilla**.

Para eliminar el origen del repositorio y evitar mandar cambios usamos el siguiente comando
```
git remote rm origin
```
Al crear le nuevo repositorio en el cual se valla utilizar esta plantilal nos da las instrucciónes para agregar el nuevo origen.

#### Personalizar plantilla
Para cambiar los colores utilizados por Vuetify (Tema) debemos ir al archivo de configuración _**src/plugins/vuetify.js**_, dentro de este archivo veremos las opciones para perzonalizar el tema
```
{
    light: {
        primary: '#2e3141',
        secondary: '#242734',
        accent: '#8e44ad',
        error: '#e74c3c',
        info: '#3498db',
        success: '#2ecc71',
        warning: '#f1c40f'
    }
}
```

Dentro del archivo _**src/views/Home.vue**_ vamos a encontrar algunas variables que nos ayudaran a personalizar la plantilla
```
data: () => ({
            drawer: null,
            menus: menu,
            menuWidth: 260, // Ancho de la barra lateral principal
            mainLogo: require('../assets/logo.png'), // Este es el logo que aparece en la barra lateral
            prominentLogo: true, // Logo pequeño o Grande
            logoBackColor: 'secondary', // Color de fondo para el logo de la barra lateral
            sideBackColor: 'primary', // Color de fondo de la barra lateral
            darkTheme: true, // Tema obscuro ( letras en blanco en caso de colores obscuros de la barra lateral)
            appBarColor: '', // Color de la barra principal de la app
            appBarDark: false // Tema obscuro ( letras en blanco en caso de colores obscuros de la barra principal)
        });
```

Para agregar mas elementos al menu debemos modificar el archivo _**src/utils/menu.js**_

**Falta terminar documentación**



### Como usar la plantilla
Primero que nada debemos tener **[NodeJS/NPM](https://nodejs.org/es/)** instalado

Una vez que tenemos NPM instalado utilizamos el siguiente comando para instalar las 
dependencias de la plantilla
```
npm install
```
Una vez instaladas las dependencias podemos correr el servidor prueba con hot-reaload 

```
npm run serve
```


### Para compilar y minificar todo utilizamos
```
npm run build
```

### Configuración y demas de Vuetify
Ver [Documentación de vuetify](https://cli.vuejs.org/config/).

### Enlaces de interes
* [Awesome Vue](https://github.com/vuejs/awesome-vue)
* [Awesome Vuetify](https://github.com/vuetifyjs/awesome-vuetify)
* [NPM](https://www.npmjs.com/)
